# MessageBird send/receive messages app code challenge

## DEMO: https://messagebird-messages.firebaseapp.com/

# Run the project
 1. `yarn/npm install`
 2. `yarn start/npm run start`

# Test the code
  `yarn test/npm test`
