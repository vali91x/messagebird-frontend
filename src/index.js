import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import App from './App'
import { app } from './styles/app'
import SendMessageWebhookPage from './containers/SendMessageWebhookPage'
import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div style={app}>
        <Route exact path="/" component={App}/>
        <Route path="/send/:id" component={SendMessageWebhookPage}/>
      </div>
    </Router>
  </Provider>, document.getElementById('root')
)

registerServiceWorker()
