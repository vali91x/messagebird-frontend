export const dashboardStatus = {
  display: 'flex',
  justifyContent: 'space-evenly',
  flexFlow: 'wrap'
}

export const dashboardText = {
  textAlign: 'left',
  flexGrow: '1',
  display: 'flex',
  alignItems: 'center'
}

export const dashboardButtons = {
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center'
}

export const dashboardButton = {
  margin: '5px',
  width: '170px'
}
