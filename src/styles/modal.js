export const modalStyle = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    maxWidth: '480px',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

export const modalBody = {
  marginBottom: '10px'
}

export const messageBody = {
  marginTop: '10px',
  height: '100px'
}
