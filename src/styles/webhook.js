export const sendMessageWebhookContainer = {
  margin: '0 auto',
  maxWidth: '400px'
}

export const sendMessageWebhookBtn = {
  marginTop: '10px',
  marginBottom: '10px'
}
