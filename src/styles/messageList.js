export const messageList = {
  marginTop: '40px',
  height: '70vh',
  overflowY: 'scroll'
}

export const messageColumn = {
  width: '100%'
}

export const tableHeader = {
  fontSize: '14px'
}

export const table = {
  border: '1px solid #ecf2fc'
}

export const tableCell = {
  ...table,
  padding: '10px',
  color: '#4a5669',
  textAlign: 'left'
}
