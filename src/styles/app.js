export const app = {
  textAlign: 'center',
  backgroundColor: '#f6fafd',
  height: '100%',
  padding: '50px'
}

export const appTitle = {
  marginBottom: '50px'
}

export const appContainer = {
  height: '100%',
  minHeight: '80vh',
  backgroundColor: 'white',
  border: '1px solid #ebf5ff',
  borderRadius: '6px',
  padding: '30px'
}

export const appInputField = {
  display: 'inline'
}
