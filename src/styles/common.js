export const pageText = {
  marginBottom: '30px'
}

export const nextButtonContainer = {
  marginTop: '30px'
}

export const nextButton = {
  marginLeft: '20px',
  width: '100px'
}
