import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { steps } from './reducers/ui'
import { appTitle, appContainer} from './styles/app'
import LoginPage from './containers/LoginPage'
import SignupPage from './containers/SignupPage'
import DashboardPage from './containers/DashboardPage'

import './App.css'

class App extends Component {
  render () {
    return (
      <div className="App">
        <header style={appTitle} className="app-title">
          <h1>MessageBird send/receive messages app</h1>
        </header>
        <div style={appContainer} className="app-container">
          {this.props.loading && <div>Loading</div>}
          {this.props.step === steps.LOG_IN_PAGE && <LoginPage/>}
          {this.props.step === steps.SIGN_UP_PAGE && <SignupPage/>}
          {this.props.step === steps.DASHBOARD_PAGE && <DashboardPage/>}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  step: state.ui.step,
  loading: state.ui.loading
})


App.propTypes = {
  step: PropTypes.string.isRequired,
  loading: PropTypes.bool
}

export default connect(mapStateToProps)(App)
