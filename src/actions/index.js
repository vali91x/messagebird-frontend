// import axios from 'axios'

import {
  SET_LOGIN_STATUS,
  FETCH_LOGIN_STATUS,
  GO_TO_SIGNUP,
  FETCH_SIGNUP_STATUS,
  SEND_MESSAGE_REQUEST,
  SEND_MESSAGE_SUCCESS,
  SEND_MESSAGE_FAILED,
  GET_MESSAGES_REQUEST,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILED,
  WEBHOOK_SEND_MESSAGE_REQUEST,
  WEBHOOK_SEND_MESSAGE_SUCCESS,
  WEBHOOK_SEND_MESSAGE_FAILED
} from './actionTypes'

import api from '../api'

export const fetchLoginStatus = () => ({
  type: FETCH_LOGIN_STATUS
})

export const setLoginStatus = response => ({
  type: SET_LOGIN_STATUS,
  response
})

export const goToSignup = phoneNumber => ({
  type: GO_TO_SIGNUP,
  phoneNumber
})

export const fetchSignupStatus = () => ({
  type: FETCH_SIGNUP_STATUS
})

export function loginWithPhone (phoneNumber) {
  return async dispatch => {
    dispatch(fetchLoginStatus())
    await api.loginWithPhone(phoneNumber,
      response => {
        if (response.account.name) {
          dispatch(setLoginStatus(response.account))
        } else {
          dispatch(goToSignup(phoneNumber))
        }
      },
      error => {
        console.log(error)
        dispatch(goToSignup(phoneNumber))
      })
  }
}

export function signup (name, phoneNumber) {
  return async dispatch => {
    dispatch(fetchSignupStatus())
    await api.signupWithPhoneAndName(phoneNumber, name,
      response => {
        if (response.message === 'Account created successfully') {
          dispatch(setLoginStatus(response.account))
        }
      },
      error => {
        console.log(error)
      })
  }
}

export const sendMessageRequest = () => ({
  type: SEND_MESSAGE_REQUEST
})

export const sendMessageSuccess = message => ({
  type: SEND_MESSAGE_SUCCESS,
  message
})

export const sendMessageFailed = () => ({
  type: SEND_MESSAGE_FAILED
})

export function sendMessage (phoneNumber, message, originator) {
  return async dispatch => {
    dispatch(sendMessageRequest())

    await api.sendMessageWithPhoneNumberMessageAndOriginator(phoneNumber, message, originator,
      response => {
        if (response.status === 'Sent') {
          dispatch(sendMessageSuccess(response))
        } else {
          dispatch(sendMessageFailed())
        }
      },
      () => {
        dispatch(sendMessageFailed())
      })
  }
}

export const getMessagesRequest = () => ({
  type: GET_MESSAGES_REQUEST
})

export const getMessagesSuccess = (messages) => ({
  type: GET_MESSAGES_SUCCESS,
  messages
})

export const getMessagesFailed = () => ({
  type: GET_MESSAGES_FAILED
})

export function getMessages (phoneNumber) {
  return async dispatch => {
    dispatch(getMessagesRequest())

    await api.getMessagesForPhoneNumber(phoneNumber,
      response => {
        dispatch(getMessagesSuccess(response))
      },
      () => {
        dispatch(getMessagesFailed())
      })
  }
}

export const sendMessageRequestWebhook = () => ({
  type: WEBHOOK_SEND_MESSAGE_REQUEST
})

export const sendMessageSuccessWebhook = () => ({
  type: WEBHOOK_SEND_MESSAGE_SUCCESS
})

export const sendMessageFailedWebhook = () => ({
  type: WEBHOOK_SEND_MESSAGE_FAILED
})

export function sendMessageWebhook (name, message, id) {
  return async dispatch => {
    dispatch(sendMessageRequestWebhook())

    await api.sendMessageFromWebhook(name, message, id,
      response => {
        if (response.status === 'Message sent') {
          dispatch(sendMessageSuccessWebhook())
        } else {
          dispatch(sendMessageFailedWebhook())
        }
      },
      () => {
        dispatch(sendMessageFailedWebhook())
      })
  }
}

