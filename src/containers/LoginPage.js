import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { loginWithPhone } from '../actions'
import { nextButton, nextButtonContainer, pageText } from '../styles/common'
import { appInputField } from '../styles/app'

class LoginPage extends Component {
  constructor (props) {
    super(props)
    this.state = { phoneNumber: '' }
    this.handleChange = this.handleChange.bind(this)
    this.handleNext = this.handleNext.bind(this)
  }

  handleChange (event) {
    this.setState({ phoneNumber: event.target.value })
  }

  handleNext (event) {
    this.props.loginWithPhone(this.state.phoneNumber)
    event.preventDefault()
  }

  render () {
    return (
      <div className="login-page">
        <div style={pageText} className="page-text">
          Welcome! Please fill in your phone number to continue
        </div>
        <form ref="form" onSubmit={this.handleNext}>
          <input type="text"
                 style={appInputField}
                 className="app-input-field form-control font-awesome"
                 placeholder=" Type in your phone number to continue"
                 onChange={this.handleChange}
          />
          <div style={nextButtonContainer} className="next-button-container">
            <button type="submit" style={nextButton} className="next-button btn-primary btn">
              Next
            </button>
          </div>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = {
  loginWithPhone: loginWithPhone
}


LoginPage.propTypes = {
  loginWithPhone: PropTypes.func.isRequired
}

export default connect(null, mapDispatchToProps)(LoginPage)
