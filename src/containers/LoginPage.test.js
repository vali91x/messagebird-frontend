import React from 'react'
import ReactDOM from 'react-dom'

import configureMockStore from 'redux-mock-store'
import LoginPage from './LoginPage'
import renderer from 'react-test-renderer'

const mockStore = configureMockStore()
const store = mockStore({})

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<LoginPage store={store}/>, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('renders correctly', () => {
  const result = renderer.create(<LoginPage store={store}/>).toJSON()
  expect(result).toMatchSnapshot()
})
