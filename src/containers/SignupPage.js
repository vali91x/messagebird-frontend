import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { signup } from '../actions'
import { nextButton, nextButtonContainer, pageText } from '../styles/common'
import { appInputField } from '../styles/app'

class SignupPage extends Component {
  constructor (props) {
    super(props)
    this.state = { name: '' }
    this.handleChange = this.handleChange.bind(this)
    this.handleNext = this.handleNext.bind(this)

  }

  handleChange (event) {
    this.setState({ name: event.target.value })
  }

  handleNext (event) {
    this.props.signup(this.state.name, this.props.phoneNumber)
    event.preventDefault()
  }

  render () {
    return (
      <div className="signup-page">
        <div style={pageText} className="page-text">
          Please create an account for <strong>{this.props.phoneNumber}</strong> by providing your name in the field below
        </div>
        <form ref="form" onSubmit={this.handleNext}>
        <input type="text"
               style={appInputField}
               className="app-input-field form-control font-awesome"
               placeholder=" Type in your name to continue"
               onChange={this.handleChange}/>
          <div style={nextButtonContainer} className="next-button-container">
            <button type="submit" style={nextButton} className="next-button btn-primary btn">
              Next
            </button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  phoneNumber: state.ui.phoneNumber
})

const mapDispatchToProps = {
  signup: signup
}


SignupPage.propTypes = {
  phoneNumber: PropTypes.string.isRequired,
  signup: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupPage)
