import React from 'react'
import ReactDOM from 'react-dom'
import configureMockStore from 'redux-mock-store'
import SignupPage from './SignupPage'
import renderer from 'react-test-renderer'

const mockStore = configureMockStore()
const store = mockStore({
  ui: {
    phoneNumber: '3232323'
  }
})

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SignupPage store={store}/>, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('renders correctly', () => {
  const result = renderer.create(<SignupPage store={store}/>).toJSON()
  expect(result).toMatchSnapshot()
})
