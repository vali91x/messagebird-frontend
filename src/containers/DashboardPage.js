import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { sendMessage, getMessages } from '../actions'
import SendMessageDialog from '../components/SendMessageDialog'
import ReceiveMessagesDialog from '../components/ReceiveMessagesDialog'
import MessageList from '../components/MessageList'
import { dashboardButton, dashboardButtons, dashboardStatus, dashboardText } from '../styles/dashboard'
import openSocket from 'socket.io-client'

const hostname = window.location.hostname === 'localhost' ? process.env.REACT_APP_BACKEND_API_DEV : process.env.REACT_APP_BACKEND_API_PROD

const socket = openSocket(hostname)

class DashboardPage extends Component {
  constructor (props) {
    super(props)
    this.state = { sendMessageDialog: false, receiveMessagesDialog: false }
    socket.on('refresh', () => {
      setTimeout(() => {this.refreshList()}, 2000)
    })
    this.openSendMessageDialog = this.openSendMessageDialog.bind(this)
    this.closeSendMessageDialog = this.closeSendMessageDialog.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.refreshList = this.refreshList.bind(this)
    this.openReceiveMessagesDialog = this.openReceiveMessagesDialog.bind(this)
    this.closeReceiveMessagesDialog = this.closeReceiveMessagesDialog.bind(this)
  }

  openSendMessageDialog () {
    this.setState({ sendMessageDialog: true })
  }

  closeSendMessageDialog () {
    this.setState({ sendMessageDialog: false })
  }

  sendMessage (phoneNumber, message) {
    this.props.sendMessage(phoneNumber, message, this.props.phoneNumber)
  }

  openReceiveMessagesDialog () {
    this.setState({ receiveMessagesDialog: true })
  }

  closeReceiveMessagesDialog () {
    this.setState({ receiveMessagesDialog: false })
  }

  refreshList () {
    this.props.getMessages(this.props.phoneNumber)
  }

  componentDidMount () {
    this.refreshList()
  }

  render () {
    return (
      <div className="dashboard-page">
        <div style={dashboardStatus} className="dashboard-status">
          <div style={dashboardText} className="dashboard-text">
            <div>
              Welcome, <strong>{this.props.name}</strong>. Your phone number
              is <strong>{this.props.phoneNumber}</strong>
            </div>
          </div>
          <div style={dashboardButtons} className="dashboard-buttons">
            <button
              style={dashboardButton}
              className="dashboard-button btn-primary btn"
              onClick={this.openSendMessageDialog}
            >Send message
            </button>
            <button
              style={dashboardButton}
              className="dashboard-button btn-primary btn"
              onClick={this.openReceiveMessagesDialog}
            >Receive messages
            </button>
            <button
              style={dashboardButton}
              className="dashboard-button btn-primary btn"
              onClick={this.refreshList}
            >Refresh list
            </button>
          </div>
        </div>

        <MessageList messages={this.props.messages} loading={this.props.loadingMessages}/>
        <SendMessageDialog show={this.state.sendMessageDialog} close={this.closeSendMessageDialog}
                           sendMessage={this.sendMessage}/>
        <ReceiveMessagesDialog show={this.state.receiveMessagesDialog} close={this.closeReceiveMessagesDialog}
                               phoneNumber={this.props.phoneNumber} id={this.props.id}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  id: state.ui.id,
  name: state.ui.name,
  phoneNumber: state.ui.phoneNumber,
  messages: state.messages.messages,
  loadingMessages: state.messages.loading
})

const mapDispatchToProps = {
  sendMessage: sendMessage,
  getMessages: getMessages
}

DashboardPage.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  phoneNumber: PropTypes.string.isRequired,
  messages: PropTypes.array.isRequired,
  loadingMessages: PropTypes.bool,
  sendMessage: PropTypes.func.isRequired,
  getMessages: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage)
