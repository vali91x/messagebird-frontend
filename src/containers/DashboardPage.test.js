import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import DashboardPage from './DashboardPage'
import renderer from 'react-test-renderer'


const middlewares = [thunk]

const mockStore = configureMockStore(middlewares)

const store = mockStore({
  ui: {
    id: 'dsfdfd',
    name: 'George',
    phoneNumber: '+40735398391'
  },
  messages: {
    messages: [
      {
        'phoneNumber': 40735398391,
        'message': 'Hello, George',
        'type': 'Received'
      },
      {
        'phoneNumber': 40723232322,
        'message': 'Hey!',
        'type': 'Sent'
      }
      , {
        'phoneNumber': 407232323222,
        'message': `How's it going?`,
        'type': 'Error'
      }],
    loading: false
  },
  sendMessage: jest.fn()
})

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<DashboardPage store={store}/>, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('renders correctly', () => {
  const result = renderer.create(<DashboardPage store={store}/>).toJSON()
  expect(result).toMatchSnapshot()
})
