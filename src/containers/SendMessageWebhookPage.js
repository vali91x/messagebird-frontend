import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { sendMessageWebhook } from '../actions'
import { appContainer, appInputField, appTitle } from '../styles/app'
import { sendMessageWebhookBtn, sendMessageWebhookContainer } from '../styles/webhook'
import { messageBody } from '../styles/modal'
import openSocket from 'socket.io-client'

const hostname = window.location.hostname === 'localhost' ? process.env.REACT_APP_BACKEND_API_DEV : process.env.REACT_APP_BACKEND_API_PROD

const socket = openSocket(hostname)

class SendMessageWebhookPage extends Component {
  constructor (props) {
    super(props)
    this.state = { name: '', message: '' }
    this.setName = this.setName.bind(this)
    this.setMessage = this.setMessage.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
  }

  setName (event) {
    this.setState({ name: event.target.value })
  }

  setMessage (event) {
    this.setState({ message: event.target.value })
  }

  sendMessage (event) {
    socket.emit('request')
    this.props.sendMessageWebhook(this.state.name, this.state.message, this.props.match.params.id)
    event.preventDefault()
  }

  render () {
    return (
      <div className="send-message-page">
        <header style={appTitle} className="app-title">
          <h1>MessageBird send/receive messages app</h1>
        </header>
        <div style={appContainer} className="app-container">
          <h3>Send a message to your friend</h3>

          <div style={sendMessageWebhookContainer} className="send-message-webhook-container">
            <form ref="form" onSubmit={this.sendMessage}>

              <input type="text"
                     style={appInputField}
                     className="app-input-field form-control font-awesome"
                     placeholder=" Type in your name or phone number"
                     onChange={this.setName}/>
              <textarea
                style={messageBody}
                className="message-body form-control"
                placeholder="Write your message here"
                onChange={this.setMessage}/>
              <button
                type="submit"
                style={sendMessageWebhookBtn}
                className="send-message-webhook-btn btn-primary btn">
                Send message
              </button>
              {this.props.loading && <div>loading</div>}
              <div>{this.props.status}</div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loading: state.webhook.loading,
  status: state.webhook.status
})

const mapDispatchToProps = {
  sendMessageWebhook: sendMessageWebhook
}

SendMessageWebhookPage.propTypes = {
  loading: PropTypes.bool,
  status: PropTypes.string,
  sendMessageWebhook: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(SendMessageWebhookPage)
