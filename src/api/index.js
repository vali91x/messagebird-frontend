import accounts from './accounts'
import messages from './messages'

export default {
  ...accounts,
  ...messages
}
