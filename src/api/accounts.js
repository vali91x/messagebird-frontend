import axios from 'axios'

const hostname = window.location.hostname === 'localhost' ? process.env.REACT_APP_BACKEND_API_DEV : process.env.REACT_APP_BACKEND_API_PROD

export default {
  loginWithPhone: async (phoneNumber, onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'GET',
        url: `${hostname}/accounts`,
        params: {
          phoneNumber: phoneNumber
        }
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  },
  signupWithPhoneAndName: async (phoneNumber, name, onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'POST',
        url: `${hostname}/accounts`,
        data: {
          phoneNumber: phoneNumber,
          name: name
        }
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  }
}
