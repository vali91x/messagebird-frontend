import axios from 'axios'

const hostname = window.location.hostname === 'localhost' ? process.env.REACT_APP_BACKEND_API_DEV : process.env.REACT_APP_BACKEND_API_PROD

export default {
  getMessagesForPhoneNumber: async (phoneNumber, onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'GET',
        url: `${hostname}/messages`,
        params: {
          phoneNumber: phoneNumber
        }
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  },
  sendMessageWithPhoneNumberMessageAndOriginator: async (phoneNumber, message, originator, onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'POST',
        url: `${hostname}/messages`,
        data: {
          originator: originator,
          phoneNumber: phoneNumber,
          message: message
        }
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  },
  sendMessageFromWebhook: async (originator, message, id, onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'POST',
        url: `${hostname}/messages/webhook`,
        data: {
          originator: originator,
          id: id,
          message: message
        }
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  }
}
