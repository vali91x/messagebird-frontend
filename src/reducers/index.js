import { combineReducers } from 'redux'
import ui from './ui'
import messages from './messages'
import webhook from './webhook'

export default combineReducers({
  ui, messages, webhook
})
