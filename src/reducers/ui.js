import { GO_TO_SIGNUP, FETCH_LOGIN_STATUS, SET_LOGIN_STATUS, FETCH_SIGNUP_STATUS } from '../actions/actionTypes'

export const steps = {
  LOG_IN_PAGE: 'LOG_IN_PAGE',
  SIGN_UP_PAGE: 'SIGN_UP_PAGE',
  DASHBOARD_PAGE: 'DASHBOARD_PAGE'
}

const initialState = {
  step: steps.LOG_IN_PAGE
}


const ui = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LOGIN_STATUS:
      return {
        ...state,
        loading: true,
      }
    case SET_LOGIN_STATUS:
      return {
        ...state,
        loading: false,
        id: action.response.id,
        phoneNumber: action.response.phoneNumber,
        name: action.response.name,
        step: steps.DASHBOARD_PAGE
      }
    case GO_TO_SIGNUP:
      return {
        ...state,
        loading: false,
        phoneNumber: action.phoneNumber,
        step: steps.SIGN_UP_PAGE
      }
    case FETCH_SIGNUP_STATUS:
      return {
        ...state,
        loading: true
      }
    default:
      return state
  }
}

export default ui
