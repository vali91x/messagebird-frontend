import {
  SEND_MESSAGE_REQUEST,
  SEND_MESSAGE_SUCCESS,
  GET_MESSAGES_REQUEST,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILED, SEND_MESSAGE_FAILED
} from '../actions/actionTypes'

const initialState = {
  messages: []
}

const messages = (state = initialState, action) => {
  switch (action.type) {
    case SEND_MESSAGE_REQUEST:
      return {
        ...state,
        loading: true,
      }
    case SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        messages: [...state.messages, {
          phoneNumber: action.message.phoneNumber,
          message: action.message.message,
          type: 'Sent'
        }]
      }
    case SEND_MESSAGE_FAILED:
      return {
        ...state,
        loading: false
      }
    case GET_MESSAGES_REQUEST:
      return {
        ...state,
        loading: true
      }
    case GET_MESSAGES_SUCCESS:
      return {
        ...state,
        loading: false,
        messages: [...state.messages, ...action.messages]
      }
    case GET_MESSAGES_FAILED:
      return {
        ...state,
        loading: false,
        messages: [...state.messages]
      }
    default:
      return state
  }
}

export default messages
