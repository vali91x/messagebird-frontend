import { WEBHOOK_SEND_MESSAGE_REQUEST, WEBHOOK_SEND_MESSAGE_SUCCESS, WEBHOOK_SEND_MESSAGE_FAILED } from '../actions/actionTypes'

const status = {
  SENT: 'Your message has been sent sucessfully!',
  SENDING: 'Sending your message, please wait...',
  ERROR: 'There was an error sending your message'
}

const webhook = (state = {}, action) => {
  switch (action.type) {
    case WEBHOOK_SEND_MESSAGE_REQUEST:
      return {
        ...state,
        loading: true,
        status: status.SENDING
      }
    case WEBHOOK_SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        status: status.SENT
      }
    case WEBHOOK_SEND_MESSAGE_FAILED:
      return {
        ...state,
        loading: false,
        status: status.ERROR
      }
    default:
      return state
  }
}

export default webhook
