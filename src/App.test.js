import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import App from './App'
import ShallowRenderer from 'react-test-renderer/shallow'

const mockStore = configureMockStore()
const store = mockStore({
  ui: {
    step: 'LOG_IN_PAGE'
  }
})

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Provider store={store}><App/></Provider>, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('renders correctly', () => {
  const renderer = new ShallowRenderer()
  const result = renderer.render(<App store={store}/>)
  expect(result).toMatchSnapshot()
})
