import React from 'react'
import Modal from 'react-modal'
import PropTypes from 'prop-types'
import { modalStyle, modalBody } from '../styles/modal'

// skip this in tests
if (process.env.NODE_ENV !== 'test') {
  Modal.setAppElement('#root')
}

const receiveMessagesLink = (id) => `${window.location.protocol}//${window.location.hostname}:${window.location.port}/send/${id}`

const ReceiveMessagesDialog = ({ show, close, phoneNumber, id }) => (
  <Modal
    isOpen={show}
    onRequestClose={close}
    style={modalStyle}
  >
    <div className="modal-header">
      <h2 className="modal-title">Receive messages</h2>
      <p>
        People can send messages to you (<strong>{phoneNumber}</strong>) by accessing the below link
      </p>
    </div>
    <div className="modal-body" style={modalBody}>
      <a
        href={receiveMessagesLink(id)}
        target="_blank"
      >
        {receiveMessagesLink(id)}
      </a>
    </div>
    <div>
    </div>
    <div className="pull-right">
      <button onClick={close}>Close</button>
    </div>
  </Modal>
)

ReceiveMessagesDialog.propTypes = {
  show: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  phoneNumber: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
}

export default ReceiveMessagesDialog
