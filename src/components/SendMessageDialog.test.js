import React from 'react'
import ReactDOM from 'react-dom'
import SendMessageDialog from './SendMessageDialog'
import ShallowRenderer from 'react-test-renderer/shallow'

const props = {
  show: true,
  close: () => {},
  sendMessage: () => {}
}

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SendMessageDialog {...props}/>, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('renders correctly', () => {
  const renderer = new ShallowRenderer()
  const result = renderer.render(<SendMessageDialog {...props}/>)
  expect(result).toMatchSnapshot()
})
