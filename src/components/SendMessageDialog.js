import React, { Component } from 'react'
import Modal from 'react-modal'
import PropTypes from 'prop-types'
import { modalStyle, modalBody, messageBody } from '../styles/modal'
import { appInputField } from '../styles/app'
import { nextButton } from '../styles/common'

// skip this in tests
if (process.env.NODE_ENV !== 'test') {
  Modal.setAppElement('#root')
}

class SendMessageDialog extends Component {
  constructor (props) {
    super(props)
    this.state = { phoneNumber: '', message: '' }
    this.handlePhoneChange = this.handlePhoneChange.bind(this)
    this.handleMessageChange = this.handleMessageChange.bind(this)
    this.handleSendMessage = this.handleSendMessage.bind(this)
  }

  handlePhoneChange (event) {
    this.setState({ phoneNumber: event.target.value })
  }

  handleMessageChange (event) {
    this.setState({ message: event.target.value })
  }

  handleSendMessage (event) {
    this.props.sendMessage(this.state.phoneNumber, this.state.message)
    this.props.close()
    event.preventDefault()
  }

  render () {
    return (
      <Modal
        isOpen={this.props.show}
        onRequestClose={this.props.close}
        style={modalStyle}
      >
        <div className="modal-header">
          <h2 className="modal-title">Send a message</h2>
          <p>
            If the message has been sent successfully, it will show up in your message list.
          </p>
        </div>
        <form ref="form" onSubmit={this.handleSendMessage}>
          <div className="modal-body" style={modalBody}>
            <input type="text"
                   style={appInputField}
                   className="app-input-field form-control font-awesome"
                   placeholder=" Phone number (incl. country code)"
                   onChange={this.handlePhoneChange}/>
            <textarea
              style={messageBody}
              className="message-body form-control"
              placeholder="Write your message here"
              onChange={this.handleMessageChange}/>
          </div>

          <div className="pull-right">
            <button onClick={this.props.close}>Cancel</button>
            <button type="submit" style={nextButton} className="next-button btn-primary btn">Send</button>
          </div>
        </form>
      </Modal>
    )
  }
}

SendMessageDialog.propTypes = {
  show: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired
}

export default SendMessageDialog
