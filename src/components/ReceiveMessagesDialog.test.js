import React from 'react'
import ReactDOM from 'react-dom'
import ReceiveMessagesDialog from './ReceiveMessagesDialog'
import ShallowRenderer from 'react-test-renderer/shallow'

const props = {
  show: true,
  close: () => {},
  phoneNumber: '2323232',
  id: 'gfgfds'
}

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ReceiveMessagesDialog {...props}/>, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('renders correctly', () => {
  const renderer = new ShallowRenderer()
  const result = renderer.render(<ReceiveMessagesDialog {...props}/>)
  expect(result).toMatchSnapshot()
})
