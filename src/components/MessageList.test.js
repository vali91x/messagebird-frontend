import React from 'react'
import ReactDOM from 'react-dom'
import MessageList from './MessageList'
import ShallowRenderer from 'react-test-renderer/shallow'

const props = {
  messages: [
    {
      'phoneNumber': 40735398391,
      'message': 'Hello, George',
      'type': 'Received'
    },
    {
      'phoneNumber': 40723232322,
      'message': 'Hey!',
      'type': 'Sent'
    }
    ,{
      'phoneNumber': 407232323222,
      'message': `How's it going?`,
      'type': 'Error'
    }],
  loading: false
}

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MessageList {...props}/>, div)
  ReactDOM.unmountComponentAtNode(div)
})

it('renders correctly', () => {
  const renderer = new ShallowRenderer()
  const result = renderer.render(<MessageList {...props}/>)
  expect(result).toMatchSnapshot()
})
