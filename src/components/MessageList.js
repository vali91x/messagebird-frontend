import React from 'react'
import PropTypes from 'prop-types'
import { messageList, messageColumn, tableHeader, tableCell, table } from '../styles/messageList'

const MessageList = ({ messages, loading }) => (
  <div style={messageList}>
    {loading && <div>Loading your messages. Please wait...</div>}
    {(!messages.length && !loading) &&
    <div>There are no messages. Send your first message by clicking on the top button</div>
    }
    <div className="messages-table-container">
      {(!loading && messages.length > 0) &&
      <table style={table} className="messages-table">
        <tbody>
        <tr>
          <th style={{ ...tableHeader, ...tableCell }}>Type</th>
          <th style={{ ...tableHeader, ...tableCell }}>Recipient</th>
          <th style={{ ...tableHeader, ...tableCell }}>Message</th>
        </tr>
        {messages.map((message, index) =>
          <tr key={index}>
            <td style={tableCell}>{message.type}</td>
            <td style={tableCell}>{message.phoneNumber}</td>
            <td style={{ ...messageColumn, ...tableCell }}>{message.message}</td>
          </tr>
        )}
        </tbody>
      </table>}
    </div>
  </div>
)

MessageList.propTypes = {
  messages: PropTypes.array.isRequired,
  loading: PropTypes.bool
}

export default MessageList
